let billAmount = document.getElementById("bill_amount").value;
let tipAmount = document.getElementById("tip_amount").value;
let splitBetween = document.getElementsByName("number_of_people")[0].value;
document.getElementById("calculate").onclick = calculateBill;

function calculateBill() {
  let billAmount = Number(document.getElementById("bill_amount").value);
  let tipAmount = Number(document.getElementById("tip_amount").value);
  let splitBetween = Number(
    document.getElementsByName("number_of_people")[0].value
  );

  let tip = billAmount * tipAmount;
  let total = ((billAmount + tip) / splitBetween).toFixed(2);

  document.getElementsByClassName("total")[0].innerHTML = `<h1>€${total}</h1>`;
}
